use {
    clap::Parser,
    enigo::{Enigo, Key, KeyboardControllable},
    std::{thread, time::Duration},
};

/// tool to type stuff automatically
#[derive(Parser, Debug, Clone)]
#[command(version)]
struct Opt {
    /// the text to type
    #[arg(short, long)]
    text: String,
    /// how many times to type
    #[arg(short, long)]
    amount: usize,
    /// how long to wait after typing before typing again (seconds)
    #[arg(short, long)]
    padding: f64,
    /// how long to wait before typing (seconds)
    #[arg(short, long)]
    delay: f64,
    /// if enabled, the enter key will be pressed every time after typing
    #[arg(short, long)]
    enter: bool,
}

fn main() {
    let opt = Opt::parse();

    let text = opt.text;
    let amount = opt.amount;
    let padding = Duration::from_secs_f64(opt.padding);
    let delay = Duration::from_secs_f64(opt.delay);
    let enter = opt.enter;

    let mut enigo = Enigo::new();

    thread::sleep(delay);
    for _ in 0..amount {
        enigo.key_sequence(&text);
        if enter {
            enigo.key_click(Key::Return);
        }
        thread::sleep(padding);
    }
}
