# autotyper

cli tool to type stuff automatically (pretty useful for spamming or whatever)

it should work on windows, macos, and linux (but only x11!)

## dependencies

the library i use for input simulation, [enigo](https://github.com/enigo-rs/enigo), may require you to install some things.
check the requirements in their readme for more info.
